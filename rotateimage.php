<!doctype html>
<html lang="en">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
            integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
            integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
        </script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
            integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
        </script>

        <title>Hello, world!</title>
    </head>

    <body>
        <script>
        $(document).ready(function() {
            var r = 0;
            $(document).on('click', '.putar', function() {
                var tipe = $(this).attr('data-tipe');
                if (tipe == 'kanan') {
                    r = r + 90;
                    if (r > 360) {
                        r = 90;
                    }
                } else {
                    r = r - 90;
                    if (r < 0) {
                        r = 270;
                    }
                }

                console.log(r);
                $('#image1').css('transform', 'rotate(' + r + 'deg)');
            });
        });
        </script>

        <button class="btn btn-primary putar" data-tipe="kiri">Putar Kiri</button>
        <img id="image1" src="https://www.byriza.com/lib/blog/1577432104.png" style="width:300px">
        <button class="btn btn-primary putar" data-tipe="kanan">Putar Kanan</button>

    </body>

</html>

<!doctype html>
<html lang="en">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
            integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
            integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
        </script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
            integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
        </script>

        <script>
        $(document).ready(function() {
            $(function() {
                $.ajax({
                    type: "POST",
                    url: "provinsi.json",
                    success: function(results) {
                        var result = JSON.parse(results);
                        for(var i = 0; i < result.length; i++){
                            $('.provinsi').append("<option value='' data-index='"+i+"'>"+result[i].provinsi+"</option>");
                        }
                    }
                });
            });
            $(document).on('change', '.provinsi', function() {
                var index = $('option:selected', this).attr('data-index');
                console.log(index);
                $.ajax({
                    type: "POST",
                    url: "provinsi.json",
                    success: function(results) {
                        $('.kota').html('<option value="">-Pilih Kota-</option>');
                        var result = JSON.parse(results);
                        var kota = result[index].kab_kot;
                        for(var i = 0; i < kota.length; i++){
                            $('.kota').append("<option value='' data-kota=''>"+kota[i].nama+"</option>");
                        }
                    }
                });
            });
        });
        </script>
        <title>Hello, world!</title>
    </head>

    <body>
        <div style="height: 100px;"></div>
        <div class="row justify-content-center">
            <div class="col-md-6">
                <form method="post">
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Provinsi</label>
                        <div class="col-sm-8">
                            <select name="id_provinsi" class="form-control provinsi" required>
                                <option value="">-Pilih Provinsi-</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Kota</label>
                        <div class="col-sm-8">
                            <select name="id_kota" class="form-control kota" required>
                                
                            </select>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </body>

</html>

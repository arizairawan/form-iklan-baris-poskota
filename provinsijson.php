<script>
$(document).ready(function() {
    $(function() {
        $.ajax({
            type: "POST",
            url: "provinsi.json",
            success: function(results) {
                var result = JSON.parse(results);
                for (var i = 0; i < result.length; i++) {
                    $('.provinsi').append("<option value='' data-index='" + i + "'>" +
                        result[i].provinsi + "</option>");
                }
            }
        });
    });
    $(document).on('change', '.provinsi', function() {
        var index = $('option:selected', this).attr('data-index');
        console.log(index);
        $.ajax({
            type: "POST",
            url: "provinsi.json",
            success: function(results) {
                $('.kota').html('<option value="">-Pilih Kota-</option>');
                var result = JSON.parse(results);
                var kota = result[index].kab_kot;
                for (var i = 0; i < kota.length; i++) {
                    $('.kota').append("<option value='' data-kota=''>" + kota[i].nama +
                        "</option>");
                }
            }
        });
    });
});
</script>

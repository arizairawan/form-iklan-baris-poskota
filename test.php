<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
    </script>

    <title>Hello, world!</title>
</head>

<body>
    <div style="height: 100px;"></div>
    <div class="row justify-content-center">
        <div class="col-md-6">
            <form method="post">
                <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Tipe Iklan</label>
                    <div class="col-sm-8 ">
                        <input type="radio" name="tipe" value="kontrak" required> Kontrak
                        <input type="radio" name="tipe" value="nonkontrak" required checked> Non Kontrak
                    </div>
                </div>
                <script>
                    $(document).ready(function() {
                        $(document).on('change', 'input[name="tipe"]', function() {
                            var tipe = $(this).val();
                            if (tipe == 'kontrak') {
                                $('#formkriteria').css('display', 'block');
                                $('#edisi input').attr('readonly', false);
                                $('input[name="kriteria"]').eq(0).prop('checked', true);
                                $('#more_konten').html('');
                                $('#btnaddkonten').css('display', 'block');
                            } else {
                                $('#formkriteria').css('display', 'none');
                                $('#edisi input').attr('readonly', true);
                                $('#more_terbit').html('');
                                $('#btnaddtgl').css('display', 'none');
                                $('#more_konten').html('');
                                $('#btnaddkonten').css('display', 'block');
                            }
                        });
                    });
                </script>

                <div id="formkriteria" style="display:none">
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Kriteria Iklan</label>
                        <div class="col-sm-8">
                            <input type="radio" name="kriteria" value="1" required checked> Satu Tanggal Banyak
                            Iklan <br>
                            <input type="radio" name="kriteria" value="2"> Banyak Tanggal Satu Iklan
                        </div>
                    </div>
                </div>
                <script>
                    $(document).ready(function() {
                        $(document).on('change', 'input[name="kriteria"]', function() {
                            var tipe = $(this).val();
                            if (tipe == '2') {
                                $('#btnaddtgl').css('display', 'block');
                                $('#btnaddkonten').css('display', 'none');
                                $('#more_konten').html('');
                            } else {
                                $('#btnaddtgl').css('display', 'none');
                                $('#more_terbit').html('');
                                $('#btnaddkonten').css('display', 'block');
                            }
                        });
                    });
                </script>

                <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Edisi Terbit</label>
                    <div class="col-sm-8 ">
                        <div id="edisi">
                            <input type="date" name="edisi_terbit[]" value="<?php echo date('Y-m-d', strtotime(date('Y-m-d') . ' +1 day')); ?>" class="form-control formedisi" value="" readonly style="margin-bottom:20px">
                        </div>
                        <div id="more_terbit">

                        </div>
                    </div>
                    <div class="col-sm-12 text-right" id="btnaddtgl" style="display:none">
                        <a href="javascript:void(0)" id="addtgl" class="btn btn-primary">+</a>
                        <a href="javascript:void(0)" id="mintgl" class="btn btn-primary">-</a>
                    </div>
                </div>
                <script>
                    $(document).ready(function() {
                        $(document).on('click', '#addtgl', function() {
                            $('#edisi input').clone().appendTo("#more_terbit");
                            var indextgl = $('.formedisi').length - 1;
                            $('.formedisi').eq(indextgl).val('');
                        });
                        $(document).on('click', '#mintgl', function() {
                            var indextgl = $('.formedisi').length - 1;
                            if (indextgl > 0) {
                                $('.formedisi').eq(indextgl).remove();
                            }
                        });
                    });
                </script>
                <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Agen/Biro</label>
                    <div class="col-sm-8 ">
                        Agen
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Nama Pemasang</label>
                    <div class="col-sm-8 ">
                        Agen Jakarta barat
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Diskon</label>
                    <div class="col-sm-8 ">
                        20%
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Konten</label>
                    <div class="col-sm-8">
                        <div id="konten">

                            <select name="id_kategori" class="form-control kategori" required>
                                <option value="" data-char="0">-Pilih Kategori-</option>
                                <option value="" data-char="260" data-char-on-line="26" data-min-line="5" data-price="12000" data-sub="">Kategori 1</option>
                                <option value="" data-char="260" data-char-on-line="26" data-min-line="5" data-price="12000" data-sub="mobil-dijual">Mobil Dijual</option>
                                <option value="" data-char="260" data-char-on-line="26" data-min-line="5" data-price="12000" data-sub="">Mobil Dicari</option>
                                <option value="" data-char="260" data-char-on-line="26" data-min-line="5" data-price="12000" data-sub="motor-dijual">Motor Dijual</option>
                                <option value="" data-char="300" data-char-on-line="26" data-min-line="5" data-price="12000" data-sub="">Motor Dicari</option>
                                <option value="" data-char="300" data-char-on-line="26" data-min-line="5" data-price="13000" data-sub="rumah">Rumah Dijual</option>
                            </select>


                            <div class="opsisubs" style="margin-top:10px;"></div>


                            <textarea name="konten[]" class="form-control formkonten" rows="5" style="margin-bottom:10px; margin-top:10px" maxlength=0></textarea>
                            <!-- udpate 14 Feb -->
                            <input type="text" class="prefix-data" readonly>
                            <!-- udpate 14 Feb -->
                            

                            <p class="countchar">0 dari 0 karakter</p>

                            Preview
                            <div class="previewtext" style="background:#F0F0F0; padding:5px"></div>
                            <br>

                            <a href="javascript:void(0)" class="btn btn-sm btn-danger deletebyindex">Hapus</a>
                            <hr>
                        </div>
                        <div id="more_konten"></div>
                    </div>
                    <div class="col-sm-12 text-right" id="btnaddkonten">
                        <a href="javascript:void(0)" id="addkonten" class="btn btn-primary">+</a>
                        <a href="javascript:void(0)" id="minkonten" class="btn btn-primary">-</a>
                    </div>

                    <button class="btn btn-success" disabled="disabled" id="buttonlanjut">Lanjutkan</button>
                </div>
                <script>
                    $(document).ready(function() {
                        $(document).on('click', '#addkonten', function() {
                            $('#konten').clone().appendTo("#more_konten");
                            var indexkonten = $('.formkonten').length - 1;
                            $('.formkonten').eq(indexkonten).val('');
                            $('.formkonten').eq(indexkonten).attr('maxlength', '0');
                            $('.formkonten').eq(indexkonten).attr('data-maxlength', '0');
                            $('.kategori').eq(indexkonten).val('');
                            $('.countchar').eq(indexkonten).text('0 dari 0 karakter');
                            $('.opsisubs').eq(indexkonten).html('');
                            $('.previewtext').eq(indexkonten).html('');
                        });

                        $(document).on('click', '#minkonten', function() {
                            var indexkonten = $('.formkonten').length - 1;
                            if (indexkonten > 0) {
                                $('.countchar').eq(indexkonten).text('0 dari 0 karakter');
                                $('.formkonten').eq(indexkonten).parent().remove();
                            }
                        });

                        $(document).on("change", ".kategori", function() {
                            var maxxx = $("option:selected", this).attr('data-char');

                            var price = $("option:selected", this).attr('data-price');
                            $(this).parent().find('textarea').attr('data-price', price);
                            var charonline = $("option:selected", this).attr('data-char-on-line');
                            $(this).parent().find('textarea').attr('data-char-on-line', charonline);
                            var minline = $("option:selected", this).attr('data-min-line');
                            $(this).parent().find('textarea').attr('data-min-line', minline);


                            $(this).parent().find('textarea').attr('maxlength', maxxx);

                            $(this).parent().find('textarea').attr('data-maxlength', maxxx);

                            var currentval = $(this).parent().find('textarea').val().slice(0, maxxx);
                            $(this).parent().find('textarea').val(currentval);
                            var countchar = currentval.length;

                            var subs = $("option:selected", this).attr('data-sub');

                            if (subs == 'mobil-dijual') {
                                $(this).parent().find('.opsisubs').load('listmobil.php');
                            } else if (subs == 'motor-dijual') {
                                $(this).parent().find('.opsisubs').load('listmobil.php');
                            } else if (subs == 'rumah') {
                                $(this).parent().find('.opsisubs').load('listmobil.php');
                                // update 14 Feb
                                $(this).parent().parent().find('.prefix-data').val('');
                                // update 14 Feb
                            } else {
                                $(this).parent().find('.opsisubs').html('<input type="hidden" name="sub[]">');
                                // update 14 Feb
                                $(this).parent().parent().find('.prefix-data').val('');
                                // update 14 Feb
                            }

                            $(this).parent().find('.countchar').text(countchar + ' dari ' + maxxx + ' karakter');
                        });

                        // update 14 Feb
                        $(document).on("change", ".opsisubs select", function() {
                            console.log($("option:selected", this).text());
                            var textvalue = $("option:selected", this).text();

                            var formkonten = $(this).parent().parent().find('.formkonten');
                            $(this).parent().parent().find('.prefix-data').val(textvalue+ ' ');

                            var konten = formkonten.val();
                            var preview = textvalue + ' ' + konten;

                            var previewinarray = preview.split(' ');
                            previewinarray[0] = "<b>" + previewinarray[0].toUpperCase() + "</b>";
                            previewinarray[1] = "<b>" + previewinarray[1].toUpperCase() + "</b>";
                            

                            var countchar = preview.length;
                            var maxchar = formkonten.attr('data-maxlength');
                            $(this).parent().parent().find('.previewtext').html(previewinarray.join(' ').substring(0, maxchar + 7));

                            $(this).parent().parent().find('.formkonten').val().slice(0, maxchar);
                            $(this).parent().parent().find('.countchar').text(countchar + ' dari ' + maxchar + ' karakter');
                        });
                        // update 14 Feb

                        var hasilcek = [];


                        // update 14 Feb
                        $(document).on("keyup", ".formkonten", function(e) {
                            var konten = $(this).val();
                            var indexxx = $('.formkonten').index(this);
                            var prefixdata = $('.prefix-data').eq(indexxx).val();
                            konten = prefixdata + konten;
                            console.log("ketik " + e.keyCode);
                            if (e.keyCode == 13 && !e.shiftKey) {
                                $('.formkonten').eq(indexxx).val(konten.replace(/[\r\n]+/g, ""));
                            } else {
                                checkcharkonten(e, konten, indexxx);
                            }
                        });
                        // update 14 Feb

                        $(document).on("paste", '.formkonten', function(e) {
                            // var pastedData = e.originalEvent.clipboardData.getData('text');
                            var konten = $(this).val();
                            var indexxx = $('.formkonten').index(this);
                            console.log(konten);
                            
                            checkcharkonten(e, konten, indexxx);
                        });

                        function checkcharkonten(e, konten, indexxx) {
                            var countchar = konten.length;
                            var maxchar = $('.formkonten').eq(indexxx).attr('data-maxlength');

                            console.log("index ke-" + indexxx);
                            
                            // update 14 Feb
                            var prefixdata = $('.prefix-data').eq(indexxx).val();
                            console.log('previx ' + prefixdata);
                            konten = prefixdata + konten;
                            // update 14 Feb

                            var strings = konten;

                            var capital = 0;

                            strings = strings.replace(/(^\s*)|(\s*$)/gi, ""); //exclude  start and end white-space
                            strings = strings.replace(/[ ]{2,}/gi, " "); //2 or more space to 1
                            strings = strings.replace(/\n /, "\n"); // exclude newline with a start spacing

                            var wordarray = strings.split(' ');
                            var previewarray = strings.split(' ');
                            var jmlkata = wordarray.length;

                            var i = 0;

                            var longwords = [];
                            var longestword = '';
                            var longestwordmessage = '';
                            var character = '';
                            var maxcharperkata = 25;
                            var maxcharkapital = maxchar / 3;
                            var maxcharkapitalmessage = '';

                            var pricethis = $('.formkonten').eq(indexxx).attr('data-price');
                            var charonline = $('.formkonten').eq(indexxx).attr('data-char-on-line');
                            var minline = $('.formkonten').eq(indexxx).attr('data-min-line');
                            
                            var currentlinetotal = Math.ceil(countchar / parseInt(charonline));

                            var currentprice = currentlinetotal * parseInt(pricethis);
                            if (currentlinetotal <= parseInt(minline)) {
                                var currentprice = parseInt(minline) * parseInt(pricethis);
                            }


                            while (i < jmlkata) {

                                var code = e.key;
                                if (i < 2) {
                                    wordarray[i] = wordarray[i].toUpperCase();

                                    previewarray[i] = "<b>" + previewarray[i].toUpperCase() + "</b>";
                                    $('.previewtext').eq(indexxx).html(previewarray.join(' ').substring(0, maxchar + 7));
                                }

                                var j = 0;
                                while (j <= wordarray[i].length) {
                                    character = wordarray[i].charAt(j);
                                    if (character.charCodeAt() >= 65 && character.charCodeAt() <= 90) {
                                        capital++;
                                        // console.log(capital + ' - ' + character);
                                    }
                                    j++;
                                }
                                
                                var listemail = ['@gmail', '@yahoo', '@outlook', '@live'];
                                var includeemail = false; 
                                for(var m = 0; m < listemail.length; m++){
                                    var email = wordarray[i].toLowerCase();
                                    includeemail = email.includes(listemail[m]);
                                    if(includeemail) {
                                        break;
                                    }
                                }
                                console.log("includes email " + includeemail)
                                if (wordarray[i].length >= maxcharperkata && includeemail == false) {
                                    longwords.push(wordarray[i]);
                                    console.log(longwords);
                                }

                                if (capital >= maxcharkapital || longwords.length > 0) {
                                    if (capital >= maxcharkapital) {
                                        maxcharkapitalmessage = 'huruf kapital melebihi ketentuan';
                                    }
                                    if (longwords.length > 0) {
                                        // longestword = wordarray[i];
                                        longestword = longwords.join(', ');
                                        longestwordmessage = '"' + longestword + '" lebih dari ' + maxcharperkata + ' karakter';
                                    }
                                    // $('.formkonten').eq(indexxx).attr('maxlength', countchar);
                                    hasilcek[indexxx] = false;
                                } else {
                                    longestword = '';
                                    longestwordmessage = '';
                                    maxcharkapitalmessage = '';
                                    // $('.formkonten').eq(indexxx).attr('maxlength', maxchar);
                                    hasilcek[indexxx] = true;
                                }

                                i++;
                            }

                            if (hasilcek.includes(false)) {
                                $('#buttonlanjut').attr('disabled', true);
                            } else {
                                $('#buttonlanjut').attr('disabled', false);
                            }


                            $('.formkonten').eq(indexxx).parent()
                                .find('.countchar')
                                .html(countchar + ' dari ' + maxchar + ' karakter | kapital : ' + capital + ' Baris : <b>' + currentlinetotal + '</b> Harga : Rp<b>' + currentprice + '</b> <br> <span style="color:red">' + longestwordmessage + ' - ' + maxcharkapitalmessage + '</span> ');
                        }

                        $(document).on("click", ".deletebyindex", function() {
                            var indexbtn = $('.deletebyindex').index(this);
                            var contentcount = $('.deletebyindex').length;
                            console.log(indexbtn);

                            if (contentcount != 1) {
                                $('.countchar').eq(indexbtn).text('0 dari 0 karakter');
                                $('.formkonten').eq(indexbtn).parent().remove();
                            }
                        });
                    });
                </script>

            </form>
        </div>
    </div>

</body>

</html>